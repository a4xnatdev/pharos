package org.nrg.xnat.analytics.pharos.matrices.services;

import java.util.List;
import java.util.Map;

public interface MatricesService {
    /**
     * Returns a map of allowed and excluded XNAT versions.
     *
     * @param appId      The ID of the application
     * @param appVersion The version of the application
     *
     * @return A map of allowed and excluded XNAT versions.
     */
    Map<String, List<String>> getByAppIdAndVersion(final String appId, final String appVersion);
}
