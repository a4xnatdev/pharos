package org.nrg.xnat.analytics.pharos.metrics.domain;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import java.util.HashMap;

public class MetricsReportSerializer extends StdSerializer<MetricsReport> {
    public MetricsReportSerializer() {
        super(MetricsReport.class);
    }

    @Override
    public void serialize(final MetricsReport value, final JsonGenerator generator, final SerializerProvider provider) throws IOException {
        generator.writeStartObject();
        generator.writeObjectField("uuid", value.getUuid());
        generator.writeStringField("version", value.getVersion());
        generator.writeStringField("ipAddress", value.getIpAddress());
        if (value.getId() != null) {
            generator.writeNumberField("id", value.getId());
        }
        if (value.getCreated() != null) {
            generator.writeObjectField("created", value.getCreated());
        }
        if (value.getCreatedBy() != null) {
            generator.writeObjectField("createdBy", value.getCreatedBy());
        }
        for (final String key : value.getExtended().keySet()) {
            generator.writeObjectField(key, MAPPER.readValue(value.getExtended().get(key), MAP_TYPE_REFERENCE));
        }
        generator.writeEndObject();
    }

    private static final ObjectMapper                           MAPPER             = new ObjectMapper();
    private static final TypeReference<HashMap<String, Object>> MAP_TYPE_REFERENCE = new TypeReference<HashMap<String, Object>>() {};
}
