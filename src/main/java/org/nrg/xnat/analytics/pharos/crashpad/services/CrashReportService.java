package org.nrg.xnat.analytics.pharos.crashpad.services;

import org.nrg.xnat.analytics.pharos.crashpad.domain.CrashReport;
import org.nrg.xnat.analytics.pharos.crashpad.domain.projections.SimplifiedCrashReport;

import java.util.List;
import java.util.Optional;

public interface CrashReportService {
    CrashReport getFullCrashReport(final long id);

    List<CrashReport> findReportsByGuid(final String guid);

    List<CrashReport> findReportsByProductName(final String productName);

    List<CrashReport> findReportsByProductNameAndProductVersion(final String productName, final String productVersion);

    SimplifiedCrashReport get(final long id);

    List<SimplifiedCrashReport> getAllSimplifiedReports();

    List<SimplifiedCrashReport> findSimplifiedReportsByGuid(final String guid);

    List<SimplifiedCrashReport> findSimplifiedReportsByProductName(final String productName);

    List<SimplifiedCrashReport> findSimplifiedReportsByProductNameAndProductVersion(final String productName, final String productVersion);

    void create(final CrashReport report);

    Optional<CrashReport> findOneByGuid(String guid);
}
