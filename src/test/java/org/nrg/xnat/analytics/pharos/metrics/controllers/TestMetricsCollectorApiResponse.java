package org.nrg.xnat.analytics.pharos.metrics.controllers;

import static java.time.ZoneOffset.UTC;
import static lombok.AccessLevel.PRIVATE;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.nrg.xnat.analytics.pharos.metrics.services.MetricsReportService;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(MetricsCollectorApi.class)
@Getter(PRIVATE)
@Accessors(prefix = "_")
public class TestMetricsCollectorApiResponse {
    TestMetricsCollectorApiResponse(final MockMvc mvc) {
        _mvc = mvc;
    }

    @Before
    public void setUp() {
        Mockito.when(_service.get()).thenReturn(MOCK_REPORTS);
        MOCK_REPORTS.forEach(report -> Mockito.when(_service.get(report.getId())).thenReturn(report));
    }

    @SuppressWarnings("unused")
    private static OffsetDateTime randomDateTime() {
        return OffsetDateTime.of(LocalDateTime.ofEpochSecond(ThreadLocalRandom.current().nextLong(START, END), 0, UTC), UTC);
    }

    // .serverTime(randomDateTime())
    private static final List<MetricsReport> MOCK_REPORTS = IntStream.range(1, 6).mapToObj(index -> MetricsReport.builder().uuid(UUID.randomUUID()).ipAddress("10.1.1." + index).version("1.7.5." + index).build()).collect(Collectors.toList());
    private static final long                START        = OffsetDateTime.of(2020, 1, 1, 0, 0, 0, 0, UTC).toEpochSecond();
    private static final long                END          = OffsetDateTime.of(2022, 1, 1, 0, 0, 0, 0, UTC).toEpochSecond();

    @MockBean
    private       MetricsReportService _service;
    private final MockMvc              _mvc;
}
