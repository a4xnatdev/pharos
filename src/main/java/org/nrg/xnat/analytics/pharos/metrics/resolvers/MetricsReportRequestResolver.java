package org.nrg.xnat.analytics.pharos.metrics.resolvers;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.micrometer.core.instrument.util.IOUtils;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.metrics.annotations.MetricsReportParam;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.nrg.xnat.analytics.pharos.resolvers.AbstractHandlerMethodArgumentResolver;
import org.springframework.web.multipart.MultipartFile;

@SuppressWarnings("unused")
@Slf4j
public class MetricsReportRequestResolver extends AbstractHandlerMethodArgumentResolver<MetricsReportParam, MetricsReport> {
    public MetricsReportRequestResolver() {
        super(MetricsReportParam.class);
        _mapper = new ObjectMapper();
    }

    @Override
    protected MetricsReport handleArgument(final HttpServletRequest request, final Map<String, String[]> parameters, final Map<String, List<MultipartFile>> files) {
        try {
            return _mapper.readValue(IOUtils.toString(request.getInputStream(), Charset.forName(request.getCharacterEncoding())), MetricsReport.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    private static final String       KEY_UUID              = "UUID";
    private static final String       KEY_VERSION           = "version";
    private static final String       KEY_IGNORE_IP_ADDRESS = "ignoreIpAddress";
    // private static final String KEY_SERVER_TIME       = "timestamp";
    // private static final String KEY_CORE_XNAT         = "coreXnat";
    private final        ObjectMapper _mapper;
}
