package org.nrg.xnat.analytics.pharos.crashpad.controllers;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.crashpad.annotations.CrashReportParam;
import org.nrg.xnat.analytics.pharos.crashpad.domain.CrashReport;
import org.nrg.xnat.analytics.pharos.crashpad.domain.projections.SimplifiedCrashReport;
import org.nrg.xnat.analytics.pharos.crashpad.services.CrashReportService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

@RestController
@RequestMapping("/crashpad")
@Data
@Accessors(prefix = "_")
@Slf4j
public class CrashpadApi {
    public CrashpadApi(final CrashReportService service) {
        _service = service;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<SimplifiedCrashReport> getCrashReports() {
        return getService().getAllSimplifiedReports();
    }

    @GetMapping(path = "id/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<SimplifiedCrashReport> getCrashReport(final @PathVariable long id) {
        return Optional.ofNullable(getService().get(id))
                       .map(report -> ResponseEntity.ok().body(report))
                       .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(path = "guid/{guid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SimplifiedCrashReport>> getCrashReportsByGuid(final @PathVariable String guid) {
        return Optional.ofNullable(getService().findSimplifiedReportsByGuid(guid))
                       .map(reports -> ResponseEntity.ok().body(reports))
                       .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(path = "id/{id}/full", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CrashReport> getFullCrashReport(final @PathVariable long id) {
        return Optional.ofNullable(getService().getFullCrashReport(id))
                       .map(report -> ResponseEntity.ok().body(report))
                       .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(path = "guid/{guid}/full", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CrashReport>> getFullCrashReportsByGuid(final @PathVariable String guid) {
        return Optional.ofNullable(getService().findReportsByGuid(guid))
                       .map(reports -> ResponseEntity.ok().body(reports))
                       .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping(consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_VALUE)
    public SimplifiedCrashReport handleCrashReport(final @CrashReportParam CrashReport report) {
        getService().create(report);
        return getService().get(report.getId());
    }

    private final CrashReportService _service;
}
