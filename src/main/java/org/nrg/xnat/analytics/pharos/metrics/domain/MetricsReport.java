package org.nrg.xnat.analytics.pharos.metrics.domain;

import static lombok.AccessLevel.PROTECTED;
import static lombok.AccessLevel.PUBLIC;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Immutable;
import org.nrg.xnat.analytics.pharos.domain.ImmutableEntity;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@EntityListeners(AuditingEntityListener.class)
@Entity
@Immutable
@Getter(PUBLIC)
@Setter(PROTECTED)
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder( {"uuid", "version", "ipAddress", "serverTime", "extended"})
@JsonSerialize(using = MetricsReportSerializer.class)
@Slf4j
public class MetricsReport extends ImmutableEntity {
    @Builder
    public MetricsReport(final Long id, final Date created, final String createdBy, final UUID uuid, final String version, final String ipAddress, final Map<String, String> extended) {
        super(id, createdBy, created);
        setUuid(uuid);
        setVersion(version);
        setIpAddress(ipAddress);
        setExtended(extended);
    }

    @NonNull
    @NotNull
    @Column(nullable = false)
    private UUID uuid;

    @NonNull
    @NotNull
    @Column(nullable = false)
    private String version;

    private String ipAddress;

    @JsonAnySetter
    void setExtended(final String key, final Object value) {
        try {
            extended.put(key, MAPPER.writeValueAsString(value));
        } catch (JsonProcessingException e) {
            log.error("A JSON processing error occurred trying to serialize the {} field: {}", key, value, e);
        }
    }

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "extended_report_parameters", joinColumns = @JoinColumn(name = "id"))
    @MapKeyColumn(name = "parameterName")
    @Column(name = "parameterValue", columnDefinition = "TEXT")
    // @JsonSerialize(keyUsing = MapSerializer.class)
    private Map<String, String> extended = new LinkedHashMap<>();

    private static final ObjectMapper MAPPER = new ObjectMapper();
}
