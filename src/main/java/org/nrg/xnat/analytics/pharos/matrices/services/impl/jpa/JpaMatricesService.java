package org.nrg.xnat.analytics.pharos.matrices.services.impl.jpa;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.nrg.xnat.analytics.pharos.matrices.data.CompatibilityMatrix;
import org.nrg.xnat.analytics.pharos.matrices.repositories.CompatibilityMatrixRepository;
import org.nrg.xnat.analytics.pharos.matrices.services.MatricesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

@Service
@Transactional
@Getter(PRIVATE)
@Setter(PRIVATE)
@Accessors(prefix = "_")
public class JpaMatricesService implements MatricesService {
    @Autowired
    public JpaMatricesService(final CompatibilityMatrixRepository repository) {
        _repository = repository;
    }

    @Override
    public Map<String, List<String>> getByAppIdAndVersion(final String appId, final String appVersion) {
        return getRepository().findOne(Example.of(CompatibilityMatrix.builder().appId(appId).appVersion(appVersion).build(),
                                                  ExampleMatcher.matchingAll().withIgnoreNullValues().withIgnoreCase()))
                              .map(CompatibilityMatrix::getMatrix)
                              .orElse(null);
    }

    private final CompatibilityMatrixRepository _repository;
}
