package org.nrg.xnat.analytics.pharos.metrics.controllers;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;
import java.util.Optional;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.metrics.annotations.MetricsReportParam;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.nrg.xnat.analytics.pharos.metrics.services.MetricsReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/metrics")
@Data
@Accessors(prefix = "_")
@Slf4j
public class MetricsCollectorApi {
    @Autowired
    public MetricsCollectorApi(final MetricsReportService service) {
        _service = service;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<MetricsReport> getMetricsReports() {
        return getService().get();
    }

    @GetMapping(path = "id/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<MetricsReport> getMetricsReport(final @PathVariable long id) {
        return Optional.ofNullable(getService().get(id))
                       .map(report -> ResponseEntity.ok().body(report))
                       .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(path = "uuid/{uuid}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MetricsReport>> getMetricsReportsByGuid(final @PathVariable String uuid) {
        return Optional.ofNullable(getService().findByUuid(uuid))
                       .map(reports -> ResponseEntity.ok().body(reports))
                       .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(path = "ip/{ipAddress}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MetricsReport>> getMetricsReportsByIpAddress(final @PathVariable String ipAddress) {
        return Optional.ofNullable(getService().findByIpAddress(ipAddress))
                       .map(reports -> ResponseEntity.ok().body(reports))
                       .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(path = "version/{version}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MetricsReport>> getMetricsReportsByVersion(final @PathVariable String version) {
        return Optional.ofNullable(getService().findByVersion(version))
                       .map(reports -> ResponseEntity.ok().body(reports))
                       .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public MetricsReport handleMetricsReport(final @MetricsReportParam MetricsReport report) {
        getService().create(report);
        return getService().get(report.getId());
    }

    private final MetricsReportService _service;
}
