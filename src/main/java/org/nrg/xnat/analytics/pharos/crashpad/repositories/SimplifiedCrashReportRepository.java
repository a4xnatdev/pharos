package org.nrg.xnat.analytics.pharos.crashpad.repositories;

import org.nrg.xnat.analytics.pharos.crashpad.domain.CrashReport;
import org.nrg.xnat.analytics.pharos.crashpad.domain.projections.SimplifiedCrashReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SimplifiedCrashReportRepository extends JpaRepository<CrashReport, Long> {
    SimplifiedCrashReport readById(final long id);

    List<SimplifiedCrashReport> findBy();

    List<SimplifiedCrashReport> findAllByGuid(final String guid);

    List<SimplifiedCrashReport> findAllByProductName(final String productName);

    List<SimplifiedCrashReport> findAllByProductNameAndProductVersion(final String productName, final String productVersion);
}
