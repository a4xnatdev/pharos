package org.nrg.xnat.analytics.pharos;

import org.nrg.xnat.analytics.pharos.crashpad.resolvers.CrashReportRequestResolver;
import org.nrg.xnat.analytics.pharos.metrics.resolvers.MetricsReportRequestResolver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@SpringBootApplication
@EnableJpaAuditing
@EnableTransactionManagement
public class PharosApplication implements WebMvcConfigurer {
    public static void main(final String[] arguments) {
        SpringApplication.run(PharosApplication.class, arguments);
    }

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addRedirectViewController("/", "/app");
        registry.addViewController("/login").setViewName("login");
    }

    @Override
    public void addArgumentResolvers(final List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(new CrashReportRequestResolver());
        resolvers.add(new MetricsReportRequestResolver());
    }
}
