package org.nrg.xnat.analytics.pharos.matrices.controllers;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnat.analytics.pharos.matrices.services.MatricesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static lombok.AccessLevel.PRIVATE;
import static lombok.AccessLevel.PROTECTED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/matrices")
@Data
@Accessors(prefix = "_")
@Getter(PROTECTED)
@Setter(PRIVATE)
@Slf4j
public class MatricesApi {
    @GetMapping(path = "{appId}/{appVersion}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, List<String>>> getCrashReport(final @PathVariable String appId, final @PathVariable String appVersion) {
        return Optional.ofNullable(getService().getByAppIdAndVersion(appId, appVersion))
                       .map(matrix-> ResponseEntity.ok().body(matrix))
                       .orElseGet(() -> ResponseEntity.notFound().build());
    }

    private final MatricesService _service;
}
