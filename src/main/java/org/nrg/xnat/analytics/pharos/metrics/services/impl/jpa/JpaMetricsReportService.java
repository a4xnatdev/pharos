package org.nrg.xnat.analytics.pharos.metrics.services.impl.jpa;

import static lombok.AccessLevel.PRIVATE;

import java.util.List;
import java.util.UUID;
import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.nrg.xnat.analytics.pharos.metrics.repositories.MetricsReportRepository;
import org.nrg.xnat.analytics.pharos.metrics.services.MetricsReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
@Getter(PRIVATE)
@Setter(PRIVATE)
@Accessors(prefix = "_")
public class JpaMetricsReportService implements MetricsReportService {
    @Autowired
    public JpaMetricsReportService(final MetricsReportRepository reportRepository) {
        _reportRepository = reportRepository;
    }

    @Override
    public void create(final MetricsReport report) {
        getReportRepository().save(report);
    }

    @Override
    public MetricsReport get(final long id) throws NoResultException {
        return getReportRepository().readById(id);
    }

    @Override
    public List<MetricsReport> get() {
        return getReportRepository().findAll();
    }

    @Override
    public List<MetricsReport> findByUuid(final String uuid) {
        return getReportRepository().findAllByUuid(UUID.fromString(uuid));
    }

    @Override
    public List<MetricsReport> findByIpAddress(final String ipAddress) {
        return getReportRepository().findAllByIpAddress(ipAddress);
    }

    @Override
    public List<MetricsReport> findByVersion(final String version) {
        return getReportRepository().findAllByVersion(version);
    }

    private final MetricsReportRepository _reportRepository;
}
