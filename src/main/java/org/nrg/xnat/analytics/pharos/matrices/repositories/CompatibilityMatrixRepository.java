package org.nrg.xnat.analytics.pharos.matrices.repositories;

import org.nrg.xnat.analytics.pharos.matrices.data.CompatibilityMatrix;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompatibilityMatrixRepository extends JpaRepository<CompatibilityMatrix, Long> {
}
