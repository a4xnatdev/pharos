package org.nrg.xnat.analytics.pharos.metrics.services;

import java.util.List;
import javax.persistence.NoResultException;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;

public interface MetricsReportService {
    void create(final MetricsReport report);

    MetricsReport get(final long id) throws NoResultException;

    List<MetricsReport> get();

    List<MetricsReport> findByUuid(final String guid);

    List<MetricsReport> findByIpAddress(final String ipAddress);

    List<MetricsReport> findByVersion(final String version);
}
