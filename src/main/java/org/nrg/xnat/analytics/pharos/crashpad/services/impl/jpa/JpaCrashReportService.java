package org.nrg.xnat.analytics.pharos.crashpad.services.impl.jpa;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.nrg.xnat.analytics.pharos.crashpad.domain.CrashReport;
import org.nrg.xnat.analytics.pharos.crashpad.domain.projections.SimplifiedCrashReport;
import org.nrg.xnat.analytics.pharos.crashpad.repositories.CrashReportRepository;
import org.nrg.xnat.analytics.pharos.crashpad.repositories.SimplifiedCrashReportRepository;
import org.nrg.xnat.analytics.pharos.crashpad.services.CrashReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static lombok.AccessLevel.PRIVATE;

@Service
@Transactional
@Getter(PRIVATE)
@Setter(PRIVATE)
@Accessors(prefix = "_")
public class JpaCrashReportService implements CrashReportService {
    @Autowired
    public JpaCrashReportService(final CrashReportRepository reportRepository, final SimplifiedCrashReportRepository simplifiedReportRepository) {
        _reportRepository = reportRepository;
        _simplifiedReportRepository = simplifiedReportRepository;
    }

    @Override
    public CrashReport getFullCrashReport(final long id) {
        return getReportRepository().readById(id);
    }

    @Override
    public List<CrashReport> findReportsByGuid(final String guid) {
        return getReportRepository().findAllByGuid(guid);
    }

    @Override
    public List<CrashReport> findReportsByProductName(final String productName) {
        return getReportRepository().findAllByProductName(productName);
    }

    @Override
    public List<CrashReport> findReportsByProductNameAndProductVersion(final String productName, final String productVersion) {
        return getReportRepository().findAllByProductNameAndProductVersion(productName, productVersion);
    }

    @Override
    public SimplifiedCrashReport get(final long id) {
        return getSimplifiedReportRepository().readById(id);
    }

    @Override
    public List<SimplifiedCrashReport> getAllSimplifiedReports() {
        return getSimplifiedReportRepository().findBy();
    }

    @Override
    public List<SimplifiedCrashReport> findSimplifiedReportsByGuid(final String guid) {
        return getSimplifiedReportRepository().findAllByGuid(guid);
    }

    @Override
    public List<SimplifiedCrashReport> findSimplifiedReportsByProductName(final String productName) {
        return getSimplifiedReportRepository().findAllByProductName(productName);
    }

    @Override
    public List<SimplifiedCrashReport> findSimplifiedReportsByProductNameAndProductVersion(final String productName, final String productVersion) {
        return getSimplifiedReportRepository().findAllByProductNameAndProductVersion(productName, productVersion);
    }

    @Override
    public void create(final CrashReport report) {
        getReportRepository().save(report);
    }

    @Override
    public Optional<CrashReport> findOneByGuid(final String guid) {
        return getReportRepository().findOne(Example.of(CrashReport.builder().guid(guid).build()));
    }

    private final CrashReportRepository           _reportRepository;
    private final SimplifiedCrashReportRepository _simplifiedReportRepository;
}
