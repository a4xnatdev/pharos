package org.nrg.xnat.analytics.pharos.metrics.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class TestMetricsReport {
    @Autowired
    public TestMetricsReport() {
        _mapper = new ObjectMapper();
    }

    @Test
    public void testMetricsReportSerialization() throws JsonProcessingException {
        final UUID          uuid   = UUID.randomUUID();
        final MetricsReport report = new MetricsReport();
        report.setUuid(uuid);
        report.setVersion("1.7.6");
        report.setIpAddress("10.1.1.170");
        report.setExtended("coreXnat", Map.of("version", "1.7.6",
                                              "projects", 32,
                                              "subjects", 1832,
                                              "experiments", 18327,
                                              "dataTypes", 32,
                                              "users", 125,
                                              "platformInfo",
                                              "CentOS 7.6, Tomcat 7.0.96, OpenJDK 64-Bit Server VM (build 25.222-b10, mixed mode), PostgreSQL 10.11",
                                              "experimentsByDataType", Map.of("xnat:mrSessionData", 4213,
                                                                              "xnat:petSessionData", 3163,
                                                                              "xnat:ctSessionData", 1712,
                                                                              "rad:radReadData", 7321,
                                                                              "xnatx:nihss", 1918),

                                              "plugins", Map.of("xnat:dqr", Map.of("version", "1.0"))));
        report.setExtended("xnatDqr", Map.of("pacsCount", 6,
                                             "totalStudiesSent", 3864,
                                             "totalStudiesReceived", 1853,
                                             "totalDataSent", "772.8 GB",
                                             "totalDataReceived", "370.6 GB",
                                             "totalQueries", 10258,
                                             "totalFailedQueries", 128,
                                             "averageResponseTimeInMs", 138));
        final String json = _mapper.writeValueAsString(report);
        assertThat(json).isNotNull();
    }

    private final ObjectMapper _mapper;
}
