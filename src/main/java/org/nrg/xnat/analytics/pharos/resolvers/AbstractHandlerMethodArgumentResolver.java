package org.nrg.xnat.analytics.pharos.resolvers;

import static lombok.AccessLevel.PROTECTED;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;
import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.nrg.xnat.analytics.pharos.crashpad.resolvers.CrashReportRequestResolver;
import org.nrg.xnat.analytics.pharos.domain.ImmutableEntity;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

@Getter(PROTECTED)
@Slf4j
public abstract class AbstractHandlerMethodArgumentResolver<A extends Annotation, E extends ImmutableEntity> implements HandlerMethodArgumentResolver {
    // TODO: Eventually use Reflection.getParameterizedTypeForClass() to get this without requiring specifying it in both class declaration and constructor.
    protected AbstractHandlerMethodArgumentResolver(final Class<? extends Annotation> annotation) {
        _annotation = annotation;
    }

    protected abstract E handleArgument(final HttpServletRequest request, final Map<String, String[]> parameters, final Map<String, List<MultipartFile>> files);

    @Override
    public boolean supportsParameter(final @Nonnull MethodParameter methodParameter) {
        return methodParameter.getParameterAnnotation(_annotation) != null;
    }

    @Override
    public E resolveArgument(final @Nonnull MethodParameter methodParameter, final ModelAndViewContainer modelAndViewContainer, final @Nonnull NativeWebRequest nativeWebRequest, final WebDataBinderFactory webDataBinderFactory) {
        final HttpServletRequest               request    = (HttpServletRequest) nativeWebRequest.getNativeRequest();
        final Map<String, String[]>            parameters = request.getParameterMap();
        final Map<String, List<MultipartFile>> files      = request instanceof MultipartRequest ? ((MultipartRequest) request).getMultiFileMap() : Collections.emptyMap();

        if (log.isInfoEnabled()) {
            log.info("Request has {} parameters:", parameters.size());
            final Set<String> parameterNames = parameters.keySet();
            parameterNames.forEach(key -> log.info(" * {}: {}", key, parameters.get(key)));
            log.info("Request has {} files:", files.size());
            final Set<String> fileNames = files.keySet();
            fileNames.forEach(key -> log.info(" * {}: {} files", key, files.get(key).size()));
        }

        return handleArgument(request, parameters, files);
    }

    protected String getJoinedParameters(final Map<String, String[]> parameters, final String keyGuid) {
        final String[] elements = parameters.get(keyGuid);
        return elements != null ? String.join(", ", elements) : "";
    }

    protected static byte[] convertMultipartFilesToArray(final List<MultipartFile> files) {
        final Byte[] bytes     = files.stream().map(AbstractHandlerMethodArgumentResolver::convertMultipartFileToList).flatMap(List::stream).toArray(Byte[]::new);
        final byte[] converted = new byte[bytes.length];
        IntStream.range(0, bytes.length).forEach(index -> converted[index] = bytes[index]);
        return converted;
    }

    private static List<Byte> convertMultipartFileToList(final MultipartFile multipartFile) {
        try {
            final byte[] bytes = multipartFile.getBytes();
            return Arrays.asList(ArrayUtils.toObject(bytes));
        } catch (IOException e) {
            log.warn("An error occurred trying to convert multipart file {} to byte array", multipartFile.getName(), e);
            return Collections.emptyList();
        }
    }

    private final Class<? extends Annotation> _annotation;
}
