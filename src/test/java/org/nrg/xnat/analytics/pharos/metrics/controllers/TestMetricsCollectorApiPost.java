package org.nrg.xnat.analytics.pharos.metrics.controllers;

import static lombok.AccessLevel.PRIVATE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.nrg.xnat.analytics.pharos.configuration.SecurityConfig.TEST_UUID;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.apache.commons.text.StringSubstitutor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nrg.xnat.analytics.pharos.configuration.SecurityConfig;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Configuration
@Getter(PRIVATE)
@Accessors(prefix = "_")
public class TestMetricsCollectorApiPost {
    @Autowired
    TestMetricsCollectorApiPost(final TestRestTemplate template) throws IOException {
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(new ClassPathResource("sample-report.json").getInputStream()))) {
            _sampleReport = StringSubstitutor.replace(reader.lines().collect(Collectors.joining("\n")), Map.of("uuid", TEST_UUID));
        }
        _template = template.withBasicAuth(TEST_UUID, "password");
        _headers = new HttpHeaders();
        _headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
    }

    @Test
    public void checkResponse() {
        final HttpEntity<String> request = new HttpEntity<>(getSampleReport(), getHeaders());
        final MetricsReport      report  = getTemplate().postForObject("http://localhost:" + getPort() + "/metrics", request, MetricsReport.class);
        assertThat(report).isNotNull().hasFieldOrPropertyWithValue("uuid", TEST_UUID);
    }

    @LocalServerPort
    private int _port;

    private final HttpHeaders      _headers;
    private final TestRestTemplate _template;
    private final String           _sampleReport;
}
