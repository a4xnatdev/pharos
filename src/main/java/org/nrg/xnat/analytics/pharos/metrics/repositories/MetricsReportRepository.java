package org.nrg.xnat.analytics.pharos.metrics.repositories;

import java.net.InetAddress;
import java.util.List;
import java.util.UUID;
import org.nrg.xnat.analytics.pharos.metrics.domain.MetricsReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MetricsReportRepository extends JpaRepository<MetricsReport, Long> {
    MetricsReport readById(final long id);

    List<MetricsReport> findAllByUuid(final UUID uuid);

    List<MetricsReport> findAllByIpAddress(final String ipAddress);

    List<MetricsReport> findAllByVersion(final String version);
}
