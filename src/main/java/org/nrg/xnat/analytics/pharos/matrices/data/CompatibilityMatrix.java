package org.nrg.xnat.analytics.pharos.matrices.data;

import lombok.*;
import org.apache.commons.lang3.ObjectUtils;
import org.nrg.xnat.analytics.pharos.domain.MutableEntity;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.*;

@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"appId", "appVersion"}))
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompatibilityMatrix extends MutableEntity {
    @Transient
    public Map<String, List<String>> getMatrix() {
        final Map<String, List<String>> matrix = new HashMap<>();
        matrix.put("allowed", ObjectUtils.defaultIfNull(getAllowed(), Collections.emptyList()));
        matrix.put("excluded", ObjectUtils.defaultIfNull(getAllowed(), Collections.emptyList()));
        return matrix;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof CompatibilityMatrix)) {
            return false;
        }
        if (!super.equals(object)) {
            return false;
        }
        final CompatibilityMatrix that = (CompatibilityMatrix) object;
        return getAppId().equals(that.getAppId()) &&
               getAppVersion().equals(that.getAppVersion()) &&
               Objects.equals(getAllowed(), that.getAllowed()) &&
               Objects.equals(getExcluded(), that.getExcluded());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAppId(), getAppVersion(), getAllowed(), getExcluded());
    }

    private String appId;

    private String appVersion;

    @ElementCollection
    private List<String> allowed;

    @ElementCollection
    private List<String> excluded;
}
