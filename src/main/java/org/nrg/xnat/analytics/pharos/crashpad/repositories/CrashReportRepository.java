package org.nrg.xnat.analytics.pharos.crashpad.repositories;

import org.nrg.xnat.analytics.pharos.crashpad.domain.CrashReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CrashReportRepository extends JpaRepository<CrashReport, Long> {
    CrashReport readById(final long id);

    List<CrashReport> findAllByGuid(final String guid);

    List<CrashReport> findAllByProductName(final String productName);

    List<CrashReport> findAllByProductNameAndProductVersion(final String productName, final String productVersion);
}
