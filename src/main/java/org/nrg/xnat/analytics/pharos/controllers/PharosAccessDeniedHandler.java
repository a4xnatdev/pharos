package org.nrg.xnat.analytics.pharos.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class PharosAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(final HttpServletRequest request, final HttpServletResponse response, final AccessDeniedException e) throws IOException {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.info("Attempt by {} to access protected resource: {}", authentication != null ? authentication.getName() : UNKNOWN, request.getRequestURI());
        response.sendRedirect(request.getContextPath() + "/access-denied");
    }

    private static final String UNKNOWN = "unknown or anonymous user";
}
