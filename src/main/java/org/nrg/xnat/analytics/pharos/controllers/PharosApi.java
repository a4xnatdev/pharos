package org.nrg.xnat.analytics.pharos.controllers;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/app")
@Data
@Accessors(prefix = "_")
@Slf4j
public class PharosApi {
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public String getCrashReport() {
        return "Hello, I'm Pharos";
    }

    @GetMapping(path = "register", produces = APPLICATION_JSON_VALUE)
    public UUID getUuid() {
        return UUID.randomUUID();
    }
}
