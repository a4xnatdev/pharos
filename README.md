# XNAT Pharos #

XNAT [Pharos](https://www.ancient.eu/Lighthouse_of_Alexandria) is a gateway server for the XNAT platform. The goal is
to provide an extensible platform to gather application metrics from both server and client applications, provide update
and alert information to users and administrators, and generally comply with the purposes of the Lighthouse of Alexandria
as described by Pliny the Elder:

> The object of it is, by the light of its fires at night, to give warning to ships, of the
> neighbouring shoals, and to point out to them the entrance of the harbour.

## Running Pharos ##

You can run Pharos either directly from the code repository or from a distribution package.

### Running from code repository ###

You can run directly from the code repository using the **gradlew** or **gradlew.bat** script:

```
$ ./gradlew bootRun
```

### Running from Pharos distribution ###

The Pharos distribution includes a **bin** folder containing the **xnat-pharos** and **xnat-pharos.bat** scripts. You 
can run this script directly, e.g. cd to the installation folder and run:

```
$ bin/xnat-pharos
``` 

You can also add the **bin** folder of your extracted Pharos distribution to your path and run just by typing the 
script name on the command line: 

```
$ xnat-pharos
``` 

### Installing Pharos as a system service ###

_TBD_

## Configuration ##

By default Pharos runs using an in-memory database. This means that data collected while the service is active is transient
and disappears when Pharos is shut down. This is useful for development and testing purposes. To stand up a production instance
(or at least one that retains data across restarts), you need to run Pharos with the production application profile.

To change the application profile, you can specify a value for the **spring.profiles.active** property. The active application 
profile controls the default values of a subset of application properties. Valid values for **spring.profiles.active** are:

* **dev** (which is the default value)
* **test**
* **prod**

As of this writing, the **dev** and **test** profiles are the same, but this is likely to change.

The most important configuration settings for the application are described in the table below.

| Setting | Description | **dev** | **test** | **prod** |
| ------- | ----------- | ------- | -------- | -------- |
| **spring.datasource.url** | Sets the datasource to use | **jdbc:h2:mem:dev;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE** | **jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE** | **jdbc:postgresql://localhost/pharos** |
| **spring.datasource.username** | Specifies the username for accessing the datasource | **sa** | **sa** | **pharos** |
| **spring.datasource.password** | Specifies the password for accessing the datasource | **''** (empty string) | **''** (empty string) | **pharos** |
| **spring.jpa.hibernate.ddl-auto** | Specifies whether schemas should be created automatically | **create-drop** | **create-drop** | **update** | 
| **spring.jpa.show-sql** | Whether generated SQL should be included in logging | **true** | **true** | **false** |

How you change the active profile and override default profile values depends on how you're running the application. This is
described in the sections below.

### Running from code repository ###

When using the **gradlew** script, you can specify properties directly on the command line using the **-D** option, has the 
form **-D**_property_=_value_. For example, to specify the active application profile:

```
./gradlew bootRun -Dspring.profiles.active=test
```

A common usage for this would be to use a different database URL and user for production purposes:

```
./gradlew bootRun -Dspring.profiles.active=prod -Dspring.datasource.url=jdbc:postgresql://db-server/pharos-prod \
                  -Dspring.datasource.username=pharos -Dspring.datasource.password=xyz1234
```

In practice you don't want to specify critical application credentials on the command line for security purposes. 

### Running from Pharos distribution ###

The easiest way to set properties when running from the Pharos distribution is to add the appropriate options to the
**XNAT_PHAROS_OPTS** environment variable:

```
$ export XNAT_PHAROS_OPTS="-Dspring.profiles.active=prod -Dspring.datasource.url=jdbc:postgresql://db-server/pharos-prod ..."
$ bin/xnat-pharos
```

## Default functions ##

Pharos should be extensible, but provides a number of default functions:

### Crashpad Collector ###

[Crashpad](https://chromium.googlesource.com/crashpad/crashpad/+/HEAD/doc/overview_design.md) is (or will be) integrated
into the [XNAT Desktop Client](https://bitbucket.org/xnatdev/xnat-desktop-client/downloads) using the [crashReporter
module](https://electronjs.org/docs/api/crash-reporter) to report application failures. As stated on the Chromium site,

> Crashpad is a library for capturing, storing and transmitting postmortem crash reports from a client to an upstream
> collection server.

For this to work, there has to be an endpoint that can receive and store the crash data. That's what the Crashpad
collector function does.

Info on integrating Crashpad into Electron apps is described here:

* [Electron CrashReporter — stay up to date if your app @#$!ed up!](https://thorsten-hans.com/electron-crashreporter-stay-up-to-date-if-your-app-fucked-up-3e9a989cd0a0)

To direct Crashpad reports to your running instance of Pharos, set the **submitURL** property to:

> <server>/crashpad 

Where _server_ is the address of your instance, including protocol and port. By default, Pharos listens on port 9000, so the
full URL on a development server would be http://localhost:9000/crashpad.
