package org.nrg.xnat.analytics.pharos.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    public static final String TEST_UUID = "da05abf7-36ba-4f56-a094-c0475222ede3";

    @Autowired
    public void setAccessDeniedHandler(final AccessDeniedHandler handler) {
        _handler = handler;
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/", "/app/**", "/crashpad", "/matrices/**", "/metrics/**", "/images/**/*", "/styles/**/*", "/js/**/*", "/webjars/**/*").permitAll()
            .anyRequest().authenticated()
            .and()
            .formLogin()
            .loginPage("/login")
            .permitAll()
            .and()
            .logout()
            .permitAll().and()
            .headers().frameOptions().disable().and()
            .httpBasic().and()
            .csrf().disable()
            .exceptionHandling()
            .accessDeniedHandler(_handler);
    }

    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
            .withUser(TEST_UUID).password(encoder().encode("password")).roles("USER").and()
            .withUser("user").password(encoder().encode("password")).roles("USER").and()
            .withUser("admin").password(encoder().encode("xnat-pharos")).roles("ADMIN");
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    private AccessDeniedHandler _handler;
}
